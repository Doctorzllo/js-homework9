let tabs = document.querySelector('.tabs')

tabs.addEventListener('click',function (event){
    document.getElementById(event.target.id).className = 'tabs-title active'

    document.querySelectorAll(".tabs-title").forEach((el) => {
        if (el.id !== event.target.id)   el.className = 'tabs-title'
    })

    document.querySelectorAll('.text').forEach((el,index) =>{
        el.className = index+1 == event.target.id ? 'text' : ' text visible'
    })

})

